alias c='cd'
alias t='tree'
alias lh='ll -alFh'

alias emacs='emacs -fh'

# Is this still necessary ?
alias tensorflow='source ${HOME}/work/tensorflow/bin/activate'
