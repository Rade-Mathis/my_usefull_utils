;;; Learn to use it

;; Delete gui toolbar
(tool-bar-mode -1)
(menu-bar-mode -99)

;; Color mode
(load-theme 'tsdh-dark)

;; Get column position
(setq column-number-mode t)

;; Highlithing matching parenthesis
(show-paren-mode 1)

;; Aumatic setting mode by extention :
(add-to-list 'auto-mode-alist '("\\.pl\\'" . prolog-mode))
(add-to-list 'auto-mode-alist '("TO.DO" . org-mode))

;; Tryina highlight the 80 column
;; Todo : find a way to automatically set the `C-u 79` stuff.
(add-to-list 'load-path "~/.emacs.d/lib/")
(require 'column-marker)
(global-set-key [?\C-c ?m] 'column-marker-1)

;; Automatically set a 79 column windows.
;; (add-to-list 'default-frame-alist (cons 'width 79))


;;; Not created by me :

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
