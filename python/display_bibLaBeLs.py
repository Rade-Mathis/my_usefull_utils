""" Given a bibTeX file, will print all the entries' label """

import sys

if len (sys.argv) < 2 :
    print ("please enter a filename via command line")
    exit (1)

try :
    bib = open (sys.argv[1])
except :
    print ("are you sure the file named \"{}\" exists ?".format (sys.argv[1]))
    exit (2)

for line in bib.readlines () :
    if    line[:8]  == "@article"      \
       or line[:14] == "@inproceedings" \
       or line[:5]  == "@misc" :
        print (line.split ('{')[1].split (',')[0])
        
