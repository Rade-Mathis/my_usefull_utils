# rm security, becaus I'm a stupid man.
alias rrm='/bin/rm'
rm ()
{
    CORBEILLE=/tmp/trash
    if [ ! -d  $CORBEILLE ]
    then
	mkdir $CORBEILLE
    fi
    
    for I in $@
    do
	if [ -a $I ]
	then
	    if [ -a $CORBEILLE/$I ]
	    then
		rrm -rf $CORBEILLE/$I
		echo "[destroyed][old] $I"
	    fi
	    mv $I $CORBEILLE/
	    echo "[moved to trash] $I"
	else
	    echo "$I not found"
	fi
    done
}

restore ()
{
    CORBEILLE=/tmp/trash
    if [ ! -d $CORBEILLE ]
    then
       echo "No trash has been found."
    else
	for I in $@
	do
	    if [ -a $CORBEILLE/$I ]
	    then
		mv $CORBEILLE/$I $PWD
		echo "[restored] $I"
	    else
		echo "No $I has been found in the trash."
	    fi
	done
    fi
}

# Comfort stuff

cl ()
{
	cd $@;
	ls;
}

mkcd ()
{
	mkdir $@ ;
	cd $@ ;
}

stfu ()
{
	$@ >/dev/null 2>&1 &
}

ltxc ()
{
    pdflatex $1 &&
    bibtex $1 &&
    pdflatex $1 &&
    pdflatex $1
}

pcsv ()  # CSV displayer
{
    USAGE="usage: pcsv <filename> [delimiter [vertical-scroll-speed]]"
    case $# in
        0) echo $USAGE ; return 1 ;;
        1) SEP=";"  ; MOV=2  ;;
        2) SEP="$2" ; MOV=2  ;;
        3) SEP="$2" ; MOV=$3 ;;
        *) echo $USAGE ; return 1 ;;
    esac
    if [ ! -r $1 ]; then
        echo "Unable to read file $1"
        return 1
    fi
    column -s"$SEP" -t < $1 | less -N -S -#$MOV
}
