import math
import sys

def print_percent (value, intro="", place=79) :
    """
    Given a value, print a 'progress bar' from 0 to 1.

    Parameters
    ----------
    value : float (between 0. and 1.)
        

    Todo
    ----
    Demander a rade_mathis[at]free_fr de finir sa putain de doc ...

    """
    using = place - len (intro) - 7
    dieses = math.floor (value * using)
    digit = str (math.floor (10 * (value * using - dieses)))

    print (end=intro)
    print (end=" ")
    for i in range (dieses) :
        print (end="#")
    print (end=digit)
    for i in range (using - dieses) :
        print (end=" ")
    print (end="{:>3} %".format (round (value * 100)))
    sys.stdout.flush ()

def clear_line (place=79) :
    """
    Clear present terminal line

    Parameters
    ----------
    place : int
        suposed number of columns in the terminal
        actually cleared columns from the left of the screen
    """
    print (end="\r")
    for i in range (place) :
        print (end=" ")
    print (end="\r")
