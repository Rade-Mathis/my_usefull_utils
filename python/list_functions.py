#!/usr/bin/python3
import sys

def usage () :
    print ("usage: python3 list_functions <python_filename>")

def main (argv) :
    if len (argv) != 2 :
        usage ()
        exit (1)

    try :
        pyfile = open (argv[1])
    except :
        print ("Failed to open {}".format (argv[1]))
        exit(1)

    for line in pyfile.readlines () :
        if "def" in line :
            state = "1st_yt"
            for char in line :
                if state == "1st_yt" :
                    if char.isspace () :
                        continue
                    elif char == "d" :
                        state = "d"
                    else :
                        break
                elif state == "d" :
                    if char == "e" :
                        state = "e"
                    else :
                        break
                elif state == "e" :
                    if char == "f" :
                        state = "f"
                    else :
                        break
                elif state == "f" :
                    if char.isspace () :
                        state = "2nd_yt"
                    else :
                        break
                elif state == "2nd_yt" :
                    if char == "(" :
                        print ()
                        break
                    else :
                        print (end=char)

                        
if __name__ == "__main__" :
    main (sys.argv)
